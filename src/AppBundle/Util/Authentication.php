<?php declare(strict_types = 1);

namespace AppBundle\Util;

use Symfony\Component\HttpFoundation\RequestStack;

class Authentication
{
    const ENV_NAME = 'SWITCH_IP_FB';

    /**
     * @var RequestStack
     */
    private $_request;

    /**
     * @var string
     */
    private $_kernelEnv;

    /**
     * @var array
     */
    private $_validIps;

    /**
     * @var array
     */
    private $_ipRanges;

    public function __construct(
        RequestStack $request,
        string $kernelEnv,
        array $validIps,
        array $ipRanges
    ) {
        $this->_kernelEnv = $kernelEnv;
        $this->_request   = $request;
        $this->_validIps  = $validIps;
        $this->_ipRanges  = $ipRanges;
    }

    /**
     * Determines if user has access for extended features.
     *
     * @return boolean
     */
    public function hasAccess() : bool
    {
        if ($this->_kernelEnv == 'prod') {
            return $this->_checkIpRange();
        }
        return getenv(self::ENV_NAME) === 'true';
    }

    private function _checkIpRange() : bool
    {
        $ipAddress = $this->_request->getMasterRequest()->getClientIp();
        $ipLong = ip2long($ipAddress);

        if ($ipLong) {
            foreach ($this->_validIps as $ipAddress) {
                if (ip2long($ipAddress) == $ipLong) {
                    return true;
                }
            }
            foreach ($this->_ipRanges as $range) {
                if (ip2long($range[0]) <= $ipLong && $ipLong <= ip2long($range[1])) {
                    return true;
                }
            }
        }

        return false;
    }
}
