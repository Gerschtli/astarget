<?php declare(strict_types = 1);

namespace AppBundle\Command;

use AppBundle\Service\Job as JobService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CleanCommand extends Command
{
    const LOG_PREFIX = 'CleanCommand';

    /**
     * Configure required credentials for clean command.
     */
    protected function configure() : void
    {
        $this
            ->setName('app:clean')
            ->setDescription('Removes old files of executed jobs.')
            ->setHelp('This command removes old files of executed jobs.');
    }

    /**
     * Removes old files of executed jobs after configured time.
     *
     * @param  InputInterface  $input  Currently unused
     * @param  OutputInterface $output Currently unused
     * @return int                     Exit code of command
     *
     * @SuppressWarnings(PMD.UnusedFormalParameter)
     */
    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $container = $this->getContainer();

        $filesystemService = $container->get('service.filesystem');
        $jobService        = $container->get('service.job');

        // Get all jobs with status executed and older than configured parameter
        $jobs = $jobService->getRemovables();

        $this->_logInfo(
            sprintf(
                '%d jobs are ready to be removed.',
                count($jobs)
            )
        );

        // Remove job directory
        foreach ($jobs as $job) {
            if ($filesystemService->removeJobDirectory($job)) {
                // Update status if successful
                $job->setStatus(JobService::STATUS_REMOVED);
                $jobService->save($job);

                $this->_logInfo('Job directory removed.', $job);
            }
        }

        return 0;
    }

    /**
     * {@inheritDoc}
     */
    protected function _getLogPrefix() : string
    {
        return self::LOG_PREFIX;
    }
}
