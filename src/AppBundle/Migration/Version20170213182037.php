<?php declare(strict_types = 1);

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * @SuppressWarnings(PMD.ShortMethodNames)
 */
class Version20170213182037 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) : void
    {
        $table = $schema->getTable('jobs');
        $table->changeColumn('hash', ['notnull' => true]);
        $table->addUniqueIndex(['hash'], 'HASH_UNIQUE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) : void
    {
        $table = $schema->getTable('jobs');
        $table->changeColumn('hash', ['notnull' => false]);
        $table->dropIndex('HASH_UNIQUE');
    }
}
