<?php declare(strict_types = 1);

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * @SuppressWarnings(PMD.ShortMethodNames)
 */
class Version20161113122110 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) : void
    {
        $table = $schema->getTable('jobs');
        $table->changeColumn('executed_at', ['notnull' => false]);
        $table->changeColumn('execution_time', ['notnull' => false]);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) : void
    {
        $table = $schema->getTable('jobs');
        $table->changeColumn('executed_at', ['notnull' => true]);
        $table->changeColumn('execution_time', ['notnull' => true]);
    }
}
