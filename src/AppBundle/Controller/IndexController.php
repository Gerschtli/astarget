<?php declare(strict_types = 1);

namespace AppBundle\Controller;

use AppBundle\Form\IndexType;
use AppBundle\Service\FastaManager;
use AppBundle\Util\Flash;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class IndexController extends Controller
{
    public function indexAction(Request $request) : Response
    {
        $form = $this->createForm(IndexType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $fastamanager = $this->get('service.fasta_manager');
            $fasta = $fastamanager->getFormFasta($form);

            if ($fasta == null) {
                $form->get('uploadfilechoice')
                    ->addError(new FormError("Please put in a FASTA Text or upload a file."));

                return $this->_renderView($form);
            }

            // validate FASTA
            $fasta = $fastamanager->correctInput($fasta);

            switch ($fastamanager->checkFasta($fasta)) {
                case FastaManager::FASTA_OK:
                    // write job to database and write fasta
                    $jobservice = $this->get('service.job');
                    $job = $jobservice->createJob($form);

                    $job->setFastaLength($fastamanager->getFastaLength($fasta));
                    $jobservice->save($job);

                    if ($fastamanager->writeFastaToFile($fasta, $job)) {
                        $this->get('util.flash')
                            ->addFlash(
                                Flash::TYPE_SUCCESS,
                                Flash::MESSAGE_JOB_CREATED
                            );
                        return $this->redirectToRoute('result', ['hash' => $job->getHash()]);
                    }
                    $this->get('util.flash')
                        ->addFlash(
                            Flash::TYPE_ERROR,
                            Flash::MESSAGE_JOB_CREATION_FAILED
                        );
                    $jobservice->remove($job);
                    break;

                case FastaManager::FASTA_INVALID:
                    $form->get('uploadfilechoice')
                        ->addError(
                            new FormError(
                                "Your chosen FASTA does not have the required format. Please look for the '?' next to"
                                . " FASTA to get more information."
                            )
                        );
                    break;

                case FastaManager::FASTA_TOO_LONG:
                    $form->get('uploadfilechoice')
                        ->addError(
                            new FormError(
                                'Your chosen FASTA is too long. FASTA must not be longer than '
                                . "{$this->getParameter('value.max_fasta_length')} characters."
                            )
                        );
                    break;
            }
        }

        return $this->_renderView($form);
    }

    private function _renderView(Form $form) : Response
    {
        return $this->render(
            'index/index.html.twig',
            [
                'adminAccess'      => $this->get('util.authentication')->hasAccess(),
                'adminMailAddress' => $this->getParameter('app.admin_mail_address'),
                'defaultGc'        => $this->getParameter('value.default_gc'),
                'defaultLength'    => $this->getParameter('value.default_length'),
                'defaultStretches' => $this->getParameter('value.default_stretches'),
                'form'             => $form->createView(),
            ]
        );
    }
}
