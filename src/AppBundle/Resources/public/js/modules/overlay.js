/**
 * Overlay module
 *
 * Provides functionality to show and hide result overlay.
 */
(function ($) {
    "use strict";

    activate();

    var counter        = 0;
    var overlayClass   = ".result--overlay";
    var overlayVisible = "result--overlay__visible";

    /**
     * Constructor of module.
     * Subscribe to public events.
     */
    function activate() {
        /**
         * Hide overlay.
         */
        $.subscribe("overlay.hide", function () {
            toggle(-1);
        });

        /**
         * Reset internal counter.
         * Used for testing.
         */
        $.subscribe("overlay.resetCounter", function () {
            counter = 0;
        });

        /**
         * Show overlay.
         */
        $.subscribe("overlay.show", function () {
            toggle(1);
        });
    }

    /**
     * Toggle overlay.
     *
     * @param {int} increment Increment for internal counter
     */
    function toggle(increment) {
        counter += increment;

        if (counter > 0) {
            $(overlayClass).addClass(overlayVisible);
        } else {
            $(overlayClass).removeClass(overlayVisible);
        }
    }

}(jQuery));
