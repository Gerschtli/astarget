/**
 * Refresh-page module
 *
 * Refreshes page in given seconds.
 */
(function ($) {
    "use strict";

    activate();

    var locationObject = window.location;
    var timeout        = -1;

    /**
     * Constructor of module.
     * Subscribe to public events.
     */
    function activate() {
        /**
         * Init timeout for refresh page.
         */
        $.subscribe("refresh-page.init", function () {
            if (timeout < 0) {
                return;
            }

            setTimeout(function () {
                locationObject.reload();
            }, timeout * 1000);
        });

        /**
         * Sets the location object.
         * Used for testing.
         */
        $.subscribe("refresh-page.location", function (data) {
            locationObject = data;
        });

        /**
         * Save timeout (in seconds).
         */
        $.subscribe("refresh-page.timeout", function (data) {
            timeout = data;
        });
    }

}(jQuery));
