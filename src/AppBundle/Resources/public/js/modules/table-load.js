/**
 * Table-load module
 *
 * Loads and parses table data.
 */
(function ($) {
    "use strict";

    activate();

    var fastaOutputUrl;

    /**
     * Constructor of module.
     * Subscribe to public events.
     */
    function activate() {
        /**
         * Load table data.
         */
        $.subscribe("table-load.load", function () {
            if (fastaOutputUrl !== undefined) {
                load();
            }
        });

        /**
         * Save fastaOutputUrl.
         */
        $.subscribe("view.fastaOutputUrl", function (data) {
            fastaOutputUrl = data;
        });
    }

    /**
     * Load table data.
     */
    function load() {
        $.publish("overlay.show");

        $.get(fastaOutputUrl, function (data) {
            var tableData = [];

            // parse data
            var lines = data.split(/\n/);

            for (var i = 0; i < lines.length - 1; i++) {
                var values = lines[i].split(/\t/);
                tableData[i] = [];

                for (var j = 0; j < values.length; j++) {
                    // convert to float if possible
                    var parsed = parseFloat(values[j]);

                    if (isNaN(parsed)) {
                        parsed = values[j];
                    }

                    tableData[i][j] = {
                        rawValue : values[j],
                        value    : parsed,
                    };
                }
            }

            $.publish("result-table.init", [tableData]);
            $.publish("overlay.hide");
        });
    }

}(jQuery));
