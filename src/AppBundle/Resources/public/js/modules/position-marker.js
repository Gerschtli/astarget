/**
 * Position-marker module
 *
 * Provides functionality to set and reset the FASTA position marker.
 *
 * Required events:
 *   - view.fastaLength       Total Length of FASTA
 *   - view.subSequenceLength Length of FASTA subsequences
 */
(function ($) {
    "use strict";

    activate();

    var boxClass = ".result--position-pointer-box";
    var tableTr = ".result--table tbody tr";
    var fastaLength;
    var subSequenceLength;

    /**
     * Constructor of module.
     * Subscribe to public events.
     */
    function activate() {
        /**
         * Init position marker.
         */
        $.subscribe("position-marker.init", function () {
            init();
        });

        /**
         * Save fastaLength.
         */
        $.subscribe("view.fastaLength", function (data) {
            fastaLength = data;
        });

        /**
         * Save subSequenceLength.
         */
        $.subscribe("view.subSequenceLength", function (data) {
            subSequenceLength = data;
        });
    }

    /**
     * Initializes DOM event handler.
     */
    function init() {
        /**
         * Set position marker.
         */
        $(tableTr).on("mouseenter", function () {
            if (fastaLength === undefined || subSequenceLength === undefined) {
                return;
            }

            var positions = $(this).find("td").eq(0).text().split(/,/);

            for (var i = positions.length - 1; i >= 0; i--) {
                set(positions[i], positions.length);
            }
        });

        /**
         * Reset position marker.
         */
        $(tableTr).on("mouseleave", function () {
            $(boxClass).empty();
        });
    }

    /**
     * Formats a float value to a CSS percentage value.
     *
     * @param  {float} value Value to format
     * @return {String}      Formatted string
     */
    function formatPercentage(value) {
        return value * 100 + "%";
    }

    /**
     * Set position marker of provided index and total count.
     *
     * @param {int} index            Index of marker
     * @param {int} totalMarkerCount Count of marker
     */
    function set(index, totalMarkerCount) {
        var backgroundColor = "rgba(240, 40, 40, " + (1 - (totalMarkerCount - 1) * 0.15) + ")";
        var left            = formatPercentage(index / fastaLength);
        var width           = formatPercentage(subSequenceLength / fastaLength);

        var span = $("<span class='result--position-pointer-marker'/>").css({
            backgroundColor: backgroundColor,
            left           : left,
            width          : width,
        });

        $(boxClass).append(span);
    }

})(jQuery);
