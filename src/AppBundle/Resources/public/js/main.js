(function ($) {
    "use strict";

    activate();

    /**
     * Constructor of module.
     * Subscribe to public events.
     */
    function activate() {
        /**
         * Init modules.
         */
        $(document).ready(init);

        /**
         * Init modules.
         * Used for testing.
         */
        $.subscribe("document.ready", init);
    }

    /**
     * Init modules.
     */
    function init() {
        if ($("body.index").length) {
            $.publish("index-form.init");
        } else if ($("body.result__new").length) {
            $.publish("fasta-load.load");
            $.publish("link-confirm.init");
            $.publish("refresh-page.timeout", 600);
            $.publish("refresh-page.init");
        } else if ($("body.result__info").length) {
            $.publish("fasta-load.load");
            $.publish("link-confirm.init");
        } else if ($("body.result").length) {
            $.publish("fasta-load.load");
            $.publish("table-load.load");
            $.publish("table-sort.init");
            $.publish("table-filter.init");
            $.publish("link-confirm.init");
        } else if ($("body.admin").length) {
            $.publish("link-confirm.init");
        }
    }

}(jQuery));
