#!/usr/bin/perl

use strict;
use warnings "all";

our $LENGTH = $ARGV[1];
unless ($LENGTH) {die();}
# Read sequences
my %seqs = fasta2hash("$ARGV[0].sub");
my %up = &parse_RNAup("$ARGV[0].up");

my ($fa_name, $seq, $fold) = &parse_RNAfold("$ARGV[0].fold");
my (undef, undef, $wfold) = &parse_RNAfold("$ARGV[0].windowfold");

my @pl = parse_PLfold($fa_name,$LENGTH);


print "#Pos\tSeq\tOCC\tHomo\tGC\tRNAup\tUnpaired\tPlfold\n";
foreach my $id (sort keys %seqs) {
	if (length($seqs{$id}) > $LENGTH) {next;}
	my $pos = $id;
	$pos =~ s/-(.*)//;
	my $cnt = $1;
	my $tmp = $seqs{$id};
	$tmp =~ s/[AT]//g;
	my $gc = int(length($tmp)/length($seqs{$id})*100);

	# Multi pos
	my $pl; my @unp;
	foreach my $cpos (split(/,/,$pos)) {
		$pl = $pl[$cpos+1];
		if ($pl ne "NA") {$pl = int(1000*$pl)/10;}

		push(@unp, int((&unpaired($cpos,$fold)+&unpaired($cpos,$wfold))/2) );
	}
	my $unp = max(@unp);

	my $homo = &isHomo($seqs{$id});

	print "$pos\t$seqs{$id}\t$cnt\t$homo\t$gc\t$up{$id}\t$unp\t$pl\n";

}

exit;

sub max {
	my $max;
	foreach (@_) {
		if (!defined($max) || $_ > $max) {$max = $_;}
	}
	return $max;
}

sub isHomo {
	my $l = 0;
	if ($_[0] =~ /(A{2,}+)/ || $_[0] =~ /(C{2,}+)/|| $_[0] =~ /(G{2,}+)/ || $_[0] =~ /(T{2,}+)/) {
		$l = length($1);
	}
	return $l;
}

sub parse_PLfold {
	my $xname = shift;
	my $pos = shift;
	$xname =~ s/\s.*//;
	open(FILE, "<$xname"."_lunp") || die("Could not open file '$xname\_lunp': $!");
	my @ss;
	while (<FILE>) {
		if ($_ =~ /#/) {next;}
		chomp;
		my @row = split(/\s+/);
		$ss[$row[0]] = $row[$pos];
	}
	close(FILE);
	return @ss;
}

sub unpaired {
	my $pos = $_[0];
	my $x = substr($_[1],$pos,$LENGTH);
	$x =~ s/\.//g;
	my $brackets = length($x);
	return int($brackets/($LENGTH*scalar(@_))*100);
}


sub parse_RNAfold {
	my $fa_name = "Unknown";
	my $fold = "";
	my $seq = "";
	open(FILE, "<$_[0]") || die("Could not open file '$_[0]': $!");
	while (<FILE>) {
		chomp;
		if ($_ =~ /^>([^ ]+)/) 			{$fa_name = $1;}
		elsif ($_ =~ /^([\.\(\)]+)\s?/) 	{$fold = $1;}
		else {
			$seq .= $_;
		}
	}
	close(FILE);
	return ($fa_name,$seq,$fold);
}

sub parse_RNAup {
	my $file = shift;
	my %scores;
	open(FILE, "<$file") || die("Could not open file '$file': $!");
	my $head = "";
	while (<FILE>) {
		chomp;
		if ($_ =~ /^>(.+)/) {$head = $1;}
		if ($_ =~ /.*\s+([0-9]+),([0-9]+)\s+:\s+[0-9]+,[0-9]+\s+\((.+?)\s/) {
			$scores{$head} = $3;
		}
	}
	close(FILE);

	return %scores;
}

#my %hash = fasta2hash($file);
sub fasta2hash {
	my $file = shift;
	my $cut = shift;
	if (!defined($cut)) {$cut = 0;}
	my %hash;
	open(FILE,"<$file") || die("fasta2hash(): Could not open file: $file\n");
	my $last_hash = "";
	while(<FILE>) {
		$_ =~ s/\r/\n/g;
		chomp;
		if ($_ =~ /^>/) {
			$_ =~ s/^>//;
			if ($cut) {
				$_ =~ s/ .*$//s;
			}
			$hash{$_} = "";
			$last_hash = $_;
			next;
		}
		$hash{$last_hash} .= $_;
	}
	close(FILE);
	return %hash;
}

