#!/usr/bin/perl

use strict;
use warnings "all";

my $fasta;
my $length = 0;
my $scripts;

foreach (@ARGV) {
    if ($_ =~ /^-length=([0-9]+)$/) {
        $length = $1;
    }
    elsif ($_ =~ /^-scripts=([A-Za-z0-9\._\-\/]+)$/) {
        $scripts = $1;
    }
    elsif ($_ =~ /^[^-]/) {
        $fasta = $_;
    }
    else {
        die("$_!?\n");
    }
}

unless ($length > 0) {
    die("Length must be greater than 0");
}

unless (-d $scripts) {
    die("Scripts directory not found");
}

unless (-e $fasta) {
    die("No fasta found");
}

################################################
# Get regular fold
################################################
print "Folding...\n";
system("RNAfold <$fasta >$fasta.fold");

################################################
# Get subsequences
################################################
print "Subseqs...\n";
system("cat $fasta >$fasta.sub; $scripts/substr_mod.pl $length <$fasta >>$fasta.sub");

################################################
# RNAup
################################################
print "RNAup...\n";
system("RNAup --interaction_first --no_output_file <$fasta.sub >$fasta.up");

################################################
# Get unpaired scores
################################################
print "Paring status...\n";
system("RNAplfold -u $length <$fasta");

################################################
# Local folding
################################################
print "Local folding...\n";
system("$scripts/fold_window.pl $fasta >$fasta.windowfold");
