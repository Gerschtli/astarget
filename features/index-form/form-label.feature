Feature: Form has correct labels

    Scenario: I see correct labels for all form elements
        Given I am on "/"
        Then  the response status code should be 200
        And   I should see "Type of input" in the "label[for='index_uploadfilechoice']" element
        And   I should see "File" in the "label[for='index_fastafile']" element
        And   I should see "FASTA" in the "label[for='index_fasta']" element
        And   I should see "Mail" in the "label[for='index_email']" element
        And   I should see "Length" in the "label[for='index_length']" element
        And   I should see "max. Mononuc Stretches" in the "label[for='index_maxMononucStretches']" element
        And   I should see "min. GC Ratio [%]" in the "label[for='index_gcRatio']" element
        And   I should see "Submit" in the "button#index_submit" element

    @fb
    Scenario: I see correct labels for all form elements
        Given I am on "/"
        Then  the response status code should be 200
        And   I should see "Type of input" in the "label[for='index_uploadfilechoice']" element
        And   I should see "File" in the "label[for='index_fastafile']" element
        And   I should see "FASTA" in the "label[for='index_fasta']" element
        And   I should see "Mail" in the "label[for='index_email']" element
        And   I should see "Length" in the "label[for='index_length']" element
        And   I should see "max. Mononuc Stretches" in the "label[for='index_maxMononucStretches']" element
        And   I should see "min. GC Ratio [%]" in the "label[for='index_gcRatio']" element
        And   I should see "Timeout [h]" in the "label[for='index_timeout']" element
        And   I should see "Submit" in the "button#index_submit" element
