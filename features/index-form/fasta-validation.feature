Feature: Form validates FASTA

    Scenario Outline: Generate errors when sending invalid fasta files
        Given I use <host> host
        And   I am on "/"
        Then  the response status code should be 200
        When  I fill in the following:
            | index[email]               | example@example.com |
            | index[length]              | 15                  |
            | index[maxMononucStretches] | 4                   |
            | index[gcRatio]             | 30                  |
        And   I select "Upload File" from "index[uploadfilechoice]"
        And   I attach the file "fasta/invalid/<file>" to "index[fastafile]"
        And   I press "Submit"
        Then  the response status code should be 200
        And   the url should match "/"
        Then  I should see "Your chosen FASTA does not have the required format. Please look for the '?' next to FASTA to get more information." in the "label[for='index_uploadfilechoice'] + ul li" element
        Examples:
            | host    | file              |
            | default | two-headlines.fna |
            | default | two-sequences.fna |
            | fb      | two-headlines.fna |
            | fb      | two-sequences.fna |

    Scenario Outline: Generate errors when sending invalid fasta texts
        Given I use <host> host
        And   I am on "/"
        Then  the response status code should be 200
        When  I fill in the following:
            | index[email]               | example@example.com |
            | index[length]              | 15                  |
            | index[maxMononucStretches] | 4                   |
            | index[gcRatio]             | 30                  |
        And   I fill in "index[fasta]" from "fasta/invalid/<file>"
        And   I press "Submit"
        Then  the response status code should be 200
        And   the url should match "/"
        Then  I should see "Your chosen FASTA does not have the required format. Please look for the '?' next to FASTA to get more information." in the "label[for='index_uploadfilechoice'] + ul li" element
        Examples:
            | host    | file              |
            | default | two-headlines.fna |
            | default | two-sequences.fna |
            | fb      | two-headlines.fna |
            | fb      | two-sequences.fna |
