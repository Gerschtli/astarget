Feature: Form saves data after successful submit and gives output to user.

    @db @files
    Scenario: I enter valid data with file upload and receive an success message.
        Given I am on "/"
        Then  the response status code should be 200
        When  I fill in the following:
            | index[email]               | example@example.com |
            | index[length]              | 15                  |
            | index[maxMononucStretches] | 4                   |
            | index[gcRatio]             | 30                  |
        And   I select "Upload File" from "index[uploadfilechoice]"
        And   I attach the file "fasta/valid.fna" to "index[fastafile]"
        When  I press "Submit"
        Then  the response status code should be 200
        And   I should see "Your request is currently queued/being processed." in the "div.flash--success" element
        And   I should see the file "1/hyp_up1.fna" with the same content as "fasta/valid.fna"
        And   I should see the job with id 1 with the following data in the database:
            #| createdAt           | NOW()               | # does not always succeed
            | email               | example@example.com |
            | executedAt          | NULL                |
            | fastaLength         | 1788                |
            | gcRatio             | 30                  |
            | id                  | 1                   |
            | length              | 15                  |
            | maxMononucStretches | 4                   |
            | startedAt           | NULL                |
            | status              | new                 |
            | timeout             | NULL                |

    @db @fb @files
    Scenario: I enter valid data with file upload and receive an success message.
        Given I am on "/"
        Then  the response status code should be 200
        When  I fill in the following:
            | index[email]               | example@example.com |
            | index[length]              | 15                  |
            | index[maxMononucStretches] | 4                   |
            | index[gcRatio]             | 30                  |
            | index[timeout]             | 40                  |
        And   I select "Upload File" from "index[uploadfilechoice]"
        And   I attach the file "fasta/valid.fna" to "index[fastafile]"
        When  I press "Submit"
        Then  the response status code should be 200
        And   I should see "Your request is currently queued/being processed." in the "div.flash--success" element
        And   I should see the file "1/hyp_up1.fna" with the same content as "fasta/valid.fna"
        And   I should see the job with id 1 with the following data in the database:
            #| createdAt           | NOW()               | # does not always succeed
            | email               | example@example.com |
            | executedAt          | NULL                |
            | fastaLength         | 1788                |
            | gcRatio             | 30                  |
            | id                  | 1                   |
            | length              | 15                  |
            | maxMononucStretches | 4                   |
            | startedAt           | NULL                |
            | status              | new                 |
            | timeout             | 40                  |


    @db @files
    Scenario: I enter valid data with text field and receive an success message.
        Given I am on "/"
        Then  the response status code should be 200
        When  I fill in the following:
            | index[email]               | example@example.com |
            | index[length]              | 15                  |
            | index[maxMononucStretches] | 4                   |
            | index[gcRatio]             | 30                  |
        And   I fill in "index[fasta]" from "fasta/valid.fna"
        When  I press "Submit"
        Then  the response status code should be 200
        And   I should see "Your request is currently queued/being processed." in the "div.flash--success" element
        And   I should see the file "1/hyp_up1.fna" with the same content as "fasta/valid.fna"
        And   I should see the job with id 1 with the following data in the database:
            #| createdAt           | NOW()               | # does not always succeed
            | email               | example@example.com |
            | executedAt          | NULL                |
            | fastaLength         | 1788                |
            | gcRatio             | 30                  |
            | id                  | 1                   |
            | length              | 15                  |
            | maxMononucStretches | 4                   |
            | startedAt           | NULL                |
            | status              | new                 |
            | timeout             | NULL                |

    @db @fb @files
    Scenario: I enter valid data with text field and receive an success message.
        Given I am on "/"
        Then  the response status code should be 200
        When  I fill in the following:
            | index[email]               | example@example.com |
            | index[length]              | 15                  |
            | index[maxMononucStretches] | 4                   |
            | index[gcRatio]             | 30                  |
            | index[timeout]             | 40                  |
        And   I fill in "index[fasta]" from "fasta/valid.fna"
        When  I press "Submit"
        Then  the response status code should be 200
        And   I should see "Your request is currently queued/being processed." in the "div.flash--success" element
        And   I should see the file "1/hyp_up1.fna" with the same content as "fasta/valid.fna"
        And   I should see the job with id 1 with the following data in the database:
            #| createdAt           | NOW()               | # does not always succeed
            | email               | example@example.com |
            | executedAt          | NULL                |
            | fastaLength         | 1788                |
            | gcRatio             | 30                  |
            | id                  | 1                   |
            | length              | 15                  |
            | maxMononucStretches | 4                   |
            | startedAt           | NULL                |
            | status              | new                 |
            | timeout             | 40                  |
