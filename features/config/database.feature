Feature: I have direct read and write access to a test database in behat tests.

    @db
    Scenario: I can insert and read the test database
        When I insert an example job with hash "ABC-TEST" in the database
        Then I should see the job with hash "ABC-TEST" in the database
