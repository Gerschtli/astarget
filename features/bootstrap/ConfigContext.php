<?php declare(strict_types = 1);

namespace Features;

use AppBundle\Entity\Job;
use Behat\Behat\Context\Context;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelDictionary;
use DateTime;
use Features\Traits\BaseUrl;
use Features\Traits\Database;
use PHPUnit\Framework\Assert;

class ConfigContext extends MinkContext implements Context
{
    use KernelDictionary;
    use BaseUrl;
    use Database;

    /**
     * @var Job
     */
    private $_job;

    /**
     * @BeforeScenario
     */
    public function resetProperties() : void
    {
        $this->_job = null;
    }

    /**
     * @Then the base url should be :baseUrl
     */
    public function theBaseUrlShouldBe(string $baseUrl) : void
    {
        Assert::assertEquals(
            $baseUrl,
            $this->getMinkParameter('base_url')
        );
    }

    /**
     * Inserts example job in database and stores a clone in private field.
     *
     * @When I insert an example job with hash :hash in the database
     */
    public function iInsertAnExampleJobWithHashInTheDatabase(string $hash) : void
    {
        $entityManager = $this->_getEntityManager();

        $job = new Job();
        $job->setCreatedAt(new DateTime('2016-11-01 10:15'));
        $job->setEmail('test@example.de');
        $job->setExecutedAt(new DateTime('2016-11-02 02:32'));
        $job->setFastaLength(1788);
        $job->setGcRatio(40);
        $job->setHash($hash);
        $job->setLength(16);
        $job->setMaxMononucStretches(3);
        $job->setStartedAt(new DateTime('2016-11-02 01:48'));
        $job->setStatus('bla');
        $job->setTimeout(48);

        $this->_job = clone $job;

        $entityManager->persist($job);
        $entityManager->flush();
    }

    /**
     * Assert that entry in database equals job in private field.
     *
     * @Then I should see the job with hash :hash in the database
     */
    public function iShouldSeeTheJobWithHashInTheDatabase(string $hash) : void
    {
        $entityManager = $this->_getEntityManager();

        $job = $entityManager
            ->getRepository('AppBundle:Job')
            ->findOneBy(['hash' => $hash]);

        Assert::assertEquals($this->_job->getCreatedAt(), $job->getCreatedAt());
        Assert::assertEquals($this->_job->getEmail(), $job->getEmail());
        Assert::assertEquals($this->_job->getExecutedAt(), $job->getExecutedAt());
        Assert::assertEquals($this->_job->getFastaLength(), $job->getFastaLength());
        Assert::assertEquals($this->_job->getGcRatio(), $job->getGcRatio());
        Assert::assertEquals($this->_job->getHash(), $job->getHash());
        Assert::assertEquals($this->_job->getLength(), $job->getLength());
        Assert::assertEquals($this->_job->getMaxMononucStretches(), $job->getMaxMononucStretches());
        Assert::assertEquals($this->_job->getStartedAt(), $job->getStartedAt());
        Assert::assertEquals($this->_job->getStatus(), $job->getStatus());
        Assert::assertEquals($this->_job->getTimeout(), $job->getTimeout());
    }
}
