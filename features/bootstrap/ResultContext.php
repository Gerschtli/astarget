<?php declare(strict_types = 1);

namespace Features;

use AppBundle\Entity\Job;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelDictionary;
use DateTime;
use Features\Traits\BaseUrl;
use Features\Traits\Database;
use Features\Traits\Files;
use PHPUnit\Framework\Assert;

class ResultContext extends MinkContext implements Context
{
    use KernelDictionary;
    use BaseUrl;
    use Database;
    use Files;

    public function __construct(string $dataPath)
    {
        $this->_setDataPath($dataPath);
    }

    /**
     * Creates job with given data.
     *
     * @Given I have a job with the following data in the database:
     */
    public function iHaveAJobWithTheFollowingDataInTheDatabase(TableNode $table) : void
    {
        $this->_updateJob(new Job(), $table);
    }

    /**
     * Copies behat test fixture to var/data.
     *
     * @Given there is a file :file with the same content as :original
     */
    public function thereIsAFileWithTheSameContentAs(string $file, string $original) : void
    {
        $this->_getFilesystem()->copy(
            $this->_getBehatFilePath($original),
            $this->_getDataFilePath($file)
        );
    }

    /**
     * Asserts that element contains contents of given behat test fixture file.
     *
     * @Then I should see the content of :original in the :element element
     */
    public function iShouldSeeTheContentOfInTheElement(string $original, string $element) : void
    {
        $originalFileContent = file_get_contents($this->_getBehatFilePath($original));
        $originalFileContent = htmlentities($originalFileContent);

        $this->assertElementContains($element, $originalFileContent);
    }

    /**
     * Hold test until the overlay hides.
     *
     * @When I wait for the overlay to hide
     */
    public function iWaitForTheOverlayToHide()
    {
        $this->getSession()
            ->wait(
                // 5 seconds timeout
                5000,
                // wait for jQuery to load
                'typeof jQuery !== "undefined"'
                // wait for ajax calls to finish
                . ' && jQuery.active === 0'
                // wait for overlay to hide
                . ' && ! jQuery(".result--overlay").hasClass("result--overlay__visible")'
            );
    }

    /**
     * Update the inserted job with provided data.
     *
     * @Given I update the inserted job with the following:
     */
    public function iUpdateTheInsertedJobWithTheFollowing(TableNode $table) : void
    {
        $job = $this->_getEntityManager()
            ->getRepository('AppBundle:Job')
            ->find(1);

        $this->_updateJob($job, $table);
    }

    private function _updateJob(Job $job, TableNode $table) : void
    {
        foreach ($table->getTable() as $row) {
            $setter = 'set' . ucfirst($row[0]);
            $value  = $row[1];

            if (preg_match('#^(\d*)$#', $value, $matches)) {
                $value = (int) $matches[1];
            } elseif (preg_match('#^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$#', $value)) {
                $value = new DateTime($value);
            }

            $job->{$setter}($value);
        }

        $entityManager = $this->_getEntityManager();
        $entityManager->persist($job);
        $entityManager->flush();
    }
}
