#!/bin/bash

RESULT=0

# lint all yaml files
php bin/console --env=test lint:yaml app                               || RESULT=$((${RESULT} + $?))
php bin/console --env=test lint:yaml ci                                || RESULT=$((${RESULT} + $?))
php bin/console --env=test lint:yaml .gitlab-ci.yml                    || RESULT=$((${RESULT} + $?))

# lint all twig files
php bin/console --env=test lint:twig app                               || RESULT=$((${RESULT} + $?))

# lint all php files
find app      -type f -name '*.php' -print0 | xargs -0 -I % php -l "%" || RESULT=$((${RESULT} + $?))
php -l bin/console                                                     || RESULT=$((${RESULT} + $?))
find features -type f -name '*.php' -print0 | xargs -0 -I % php -l "%" || RESULT=$((${RESULT} + $?))
find src      -type f -name '*.php' -print0 | xargs -0 -I % php -l "%" || RESULT=$((${RESULT} + $?))
find tests    -type f -name '*.php' -print0 | xargs -0 -I % php -l "%" || RESULT=$((${RESULT} + $?))
find web      -type f -name '*.php' -print0 | xargs -0 -I % php -l "%" || RESULT=$((${RESULT} + $?))

[[ ${RESULT} > 255 ]] && RESULT=255

exit ${RESULT}
