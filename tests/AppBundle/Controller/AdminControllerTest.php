<?php declare(strict_types = 1);

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AdminControllerTest extends WebTestCase
{
    public function testIndexAction() : void
    {
        $client    = static::createClient();
        $container = $client->getContainer();

        $container->set(
            'service.job',
            $this
                ->getMockBuilder(\AppBundle\Service\Job::class)
                ->disableOriginalConstructor()
                ->getMock()
        );
        $container->set(
            'util.authentication',
            $this
                ->getMockBuilder(\AppBundle\Util\Authentication::class)
                ->disableOriginalConstructor()
                ->getMock()
        );

        $container->get('util.authentication')
            ->expects($this->once())
            ->method('hasAccess')
            ->will($this->returnValue(true));

        $container->get('service.job')
            ->expects($this->at(0))
            ->method('getByStatus')
            ->with($this->equalTo(['active']))
            ->will($this->returnValue([]));
        $container->get('service.job')
            ->expects($this->at(1))
            ->method('getByStatus')
            ->with($this->equalTo(['executed', 'failed', 'timeout']))
            ->will($this->returnValue([]));
        $container->get('service.job')
            ->expects($this->at(2))
            ->method('getByStatus')
            ->with($this->equalTo(['new']))
            ->will($this->returnValue([]));

        $client->request(
            \Symfony\Component\HttpFoundation\Request::METHOD_GET,
            '/list'
        );

        $this->assertEquals(
            \Symfony\Component\HttpFoundation\Response::HTTP_OK,
            $client->getResponse()->getStatusCode()
        );
    }

    public function testIndexActionWhenForbidden() : void
    {
        $client    = static::createClient();
        $container = $client->getContainer();

        $container->set(
            'util.authentication',
            $this
                ->getMockBuilder(\AppBundle\Util\Authentication::class)
                ->disableOriginalConstructor()
                ->getMock()
        );

        $container->get('util.authentication')
            ->expects($this->once())
            ->method('hasAccess')
            ->will($this->returnValue(false));

        $crawler = $client->request(
            \Symfony\Component\HttpFoundation\Request::METHOD_GET,
            '/list'
        );

        $this->assertEquals(
            \Symfony\Component\HttpFoundation\Response::HTTP_FORBIDDEN,
            $client->getResponse()->getStatusCode()
        );

        $this->assertContains('Access Denied.', $crawler->text());
    }
}
