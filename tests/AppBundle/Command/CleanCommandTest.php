<?php declare(strict_types = 1);

namespace Tests\AppBundle\Command;

use AppBundle\Command\CleanCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\DependencyInjection\Container;

/**
 * @SuppressWarnings(PMD.CouplingBetweenObjects)
 */
class CleanCommandTest extends KernelTestCase
{
    /**
     * @var CleanCommand
     */
    private $_object;

    /**
     * @var CommandTester
     */
    private $_commandTester;

    public function setUp() : void
    {
        parent::setUp();

        $this->_container = new Container();

        $this->_container->set(
            'service.filesystem',
            $this
                ->getMockBuilder(\AppBundle\Service\Filesystem::class)
                ->disableOriginalConstructor()
                ->getMock()
        );
        $this->_container->set(
            'service.job',
            $this
                ->getMockBuilder(\AppBundle\Service\Job::class)
                ->disableOriginalConstructor()
                ->getMock()
        );
        $this->_container->set(
            'logger',
            $this
                ->getMockBuilder(\Symfony\Bridge\Monolog\Logger::class)
                ->disableOriginalConstructor()
                ->getMock()
        );

        $this->_object = new CleanCommand();

        self::bootKernel();

        $application = new Application(self::$kernel);
        $application->add($this->_object);

        $command = $application->find('app:clean');
        $command->setContainer($this->_container);

        $this->_commandTester = new CommandTester($command);
    }

    public function testInstance() : void
    {
        $this->assertInstanceOf(
            \AppBundle\Command\Command::class,
            $this->_object
        );
        $this->assertInstanceOf(
            \Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand::class,
            $this->_object
        );
    }

    public function testConstants() : void
    {
        $object = new \ReflectionClass($this->_object);

        $this->assertEquals(
            [
                'LOG_PREFIX' => 'CleanCommand',
            ],
            $object->getConstants()
        );
    }

    public function testExecute() : void
    {
        $job1 = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();
        $job2 = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();

        $job2
            ->expects($this->once())
            ->method('setStatus')
            ->with($this->equalTo('removed'));
        $job2
            ->expects($this->once())
            ->method('getId')
            ->will($this->returnValue(15));

        $this->_container->get('service.job')
            ->expects($this->at(0))
            ->method('getRemovables')
            ->will($this->returnValue([$job1, $job2]));
        $this->_container->get('service.job')
            ->expects($this->at(1))
            ->method('save')
            ->will($this->returnValue($job2));

        $this->_container->get('service.filesystem')
            ->expects($this->at(0))
            ->method('removeJobDirectory')
            ->with($this->equalTo($job1))
            ->will($this->returnValue(false));
        $this->_container->get('service.filesystem')
            ->expects($this->at(1))
            ->method('removeJobDirectory')
            ->with($this->equalTo($job2))
            ->will($this->returnValue(true));

        $this->_container->get('logger')
            ->expects($this->at(0))
            ->method('info')
            ->with($this->equalTo('CleanCommand: 2 jobs are ready to be removed.'));
        $this->_container->get('logger')
            ->expects($this->at(1))
            ->method('info')
            ->with(
                $this->equalTo('CleanCommand: Job directory removed.'),
                $this->equalTo(['job-id' => 15])
            );

        $this->assertEquals(0, $this->_commandTester->execute(['command' => $this->_object->getName()]));
    }

    public function testExecuteWithNoJobs() : void
    {
        $this->_container->get('service.job')
            ->expects($this->once())
            ->method('getRemovables')
            ->will($this->returnValue([]));
        $this->_container->get('service.job')
            ->expects($this->never())
            ->method('save');

        $this->_container->get('service.filesystem')
            ->expects($this->never())
            ->method('removeJobDirectory');

        $this->assertEquals(0, $this->_commandTester->execute(['command' => $this->_object->getName()]));
    }
}
