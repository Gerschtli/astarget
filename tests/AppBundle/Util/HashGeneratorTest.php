<?php declare(strict_types = 1);

namespace Tests\AppBundle\Util;

use AppBundle\Util\HashGenerator;

class HashGeneratorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var HashGenerator
     */
    private $_object;

    public function setUp() : void
    {
        parent::setUp();
        $this->_object = new HashGenerator();
    }

    public function testGenerate() : void
    {
        $this->assertEquals(23, mb_strlen($this->_object->generate()));
    }
}
