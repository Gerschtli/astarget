<?php declare(strict_types = 1);

namespace Tests\AppBundle\Util;

use AppBundle\Util\Path;

class PathTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Path
     */
    private $_object;

    public function setUp() : void
    {
        parent::setUp();
        $this->_object = new Path('var/data', 'scripts');
    }

    public function testConstants() : void
    {
        $object = new \ReflectionClass($this->_object);

        $this->assertEquals(
            [
                'FILENAME_INPUT'           => 'hyp_up1.fna',
                'FILENAME_OUTPUT'          => 'hyp_up1.csv',
                'FILENAME_OUTPUT_FILTERED' => 'hyp_up1_filtered.csv',
                'SCRIPT_GENERATOR'         => 'generate_prediction.sh',
            ],
            $object->getConstants()
        );
    }

    public function testGetFilePath() : void
    {
        $job = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();

        $job
            ->expects($this->once())
            ->method('getId')
            ->will($this->returnValue(5));

        $this->assertEquals('var/data/5/filename', $this->_object->getFilePath($job, 'filename'));
    }

    public function testGetJobDirectory() : void
    {
        $job = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();

        $job
            ->expects($this->once())
            ->method('getId')
            ->will($this->returnValue(5));

        $this->assertEquals('var/data/5', $this->_object->getJobDirectory($job));
    }

    public function testGetScriptsDirectory() : void
    {
        $this->assertEquals('scripts', $this->_object->getScriptsDirectory());
    }

    public function testGetScriptPath() : void
    {
        $this->assertEquals('scripts/filename', $this->_object->getScriptPath('filename'));
    }
}
