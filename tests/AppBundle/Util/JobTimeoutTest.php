<?php declare(strict_types = 1);

namespace Tests\AppBundle\Util;

use AppBundle\Util\JobTimeout;

class JobTimeoutTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var JobTimeout
     */
    private $_object;

    /**
     * @var int
     */
    private $_jobTimeoutHours = 10;

    public function setUp() : void
    {
        parent::setUp();
        $this->_object = new JobTimeout($this->_jobTimeoutHours);
    }

    /**
     * @dataProvider dataProviderForTestGetTimeoutInHours
     */
    public function testGetTimeoutInHours($jobTimeout, int $expected) : void
    {
        $job = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();

        $job
            ->expects($this->once())
            ->method('getTimeout')
            ->will($this->returnValue($jobTimeout));

        $this->assertEquals($expected, $this->_object->getTimeoutInHours($job));
    }

    public function dataProviderForTestGetTimeoutInHours() : array
    {
        return [
            [5, 5],
            [null, $this->_jobTimeoutHours],
        ];
    }

    /**
     * @dataProvider dataProviderForTestGetTimeoutInSeconds
     */
    public function testGetTimeoutInSeconds($jobTimeout, int $expected) : void
    {
        $job = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();

        $job
            ->expects($this->once())
            ->method('getTimeout')
            ->will($this->returnValue($jobTimeout));

        $this->assertEquals($expected, $this->_object->getTimeoutInSeconds($job));
    }

    public function dataProviderForTestGetTimeoutInSeconds() : array
    {
        return [
            [5, 5 * 3600],
            [null, $this->_jobTimeoutHours * 3600],
        ];
    }
}
