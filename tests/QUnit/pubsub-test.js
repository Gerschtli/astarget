(function ($) {
    "use strict";

    QUnit.module("pubsub", {
        beforeEach: function () {
            var result = this.result = [];
            this.createHandler = function (id) {
                return function () {
                    result.push([id].concat([].slice.call(arguments)));
                };
            };
        },
        afterEach: function () {
            $.test.afterEach();
        }
    });

    QUnit.test("subscribe", function (assert) {
        $.subscribe("order", this.createHandler("order1"));
        $.subscribe("order", this.createHandler("order2"));
        $.subscribe("order.xyz", this.createHandler("order3"));
        $.subscribe("order.abc", this.createHandler("order4"));
        $.publish("order");

        assert.deepEqual(
            this.result,
            [
                ["order1"],
                ["order2"],
                ["order3"],
                ["order4"]
            ]
        );
    });

    QUnit.test("arguments", function (assert) {
        $.subscribe("arguments", this.createHandler("arguments1"));
        $.subscribe("arguments.xyz", this.createHandler("arguments2"));
        $.subscribe("arguments.abc", this.createHandler("arguments3"));
        $.publish("arguments", [null, "test 1 2 3", undefined, 9]);

        assert.deepEqual(
            this.result,
            [
                ["arguments1", null, "test 1 2 3", undefined, 9],
                ["arguments2", null, "test 1 2 3", undefined, 9],
                ["arguments3", null, "test 1 2 3", undefined, 9]
            ]
        );
    });

}(jQuery));
