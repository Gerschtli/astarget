(function ($) {
    "use strict";

    var ths = "#qunit-fixture .result--table th";

    QUnit.module("table-sort", {
        afterEach: function () {
            $.test.afterEach();
        }
    });

    QUnit.test("activate", function (assert) {
        assert.deepEqual($.subscriptions, ["table-sort.init"]);
        assert.deepEqual($.publishings, []);
    });

    QUnit.test("sort first column asc", function (assert) {
        $.test.publish("table-sort.init");

        $(ths).eq(0).trigger("click");

        assert.strictEqual($(ths).eq(0).data("order"), 1);
        assert.strictEqual($(ths).eq(0).find("i").attr("class"), "fa fa-sort-asc");
        assert.strictEqual($(ths).eq(1).data("order"), 0);
        assert.strictEqual($(ths).eq(1).find("i").attr("class"), "fa fa-sort");
        assert.strictEqual($(ths).eq(2).data("order"), 0);
        assert.strictEqual($(ths).eq(2).find("i").attr("class"), "fa fa-sort");

        assert.deepEqual(
            $.publishings,
            [{ name : "result-table.sort", data : [0, 1] }]
        );
    });

    QUnit.test("sort first column desc", function (assert) {
        $.test.publish("table-sort.init");

        $(ths).eq(0).trigger("click");
        $(ths).eq(0).trigger("click");

        assert.strictEqual($(ths).eq(0).data("order"), -1);
        assert.strictEqual($(ths).eq(0).find("i").attr("class"), "fa fa-sort-desc");
        assert.strictEqual($(ths).eq(1).data("order"), 0);
        assert.strictEqual($(ths).eq(1).find("i").attr("class"), "fa fa-sort");
        assert.strictEqual($(ths).eq(2).data("order"), 0);
        assert.strictEqual($(ths).eq(2).find("i").attr("class"), "fa fa-sort");

        assert.deepEqual(
            $.publishings,
            [
                { name : "result-table.sort", data : [0, 1] },
                { name : "result-table.sort", data : [0, -1] },
            ]
        );
    });

    QUnit.test("sort first and second column asc", function (assert) {
        $.test.publish("table-sort.init");

        $(ths).eq(0).trigger("click");
        $(ths).eq(1).trigger("click");

        assert.strictEqual($(ths).eq(0).data("order"), 0);
        assert.strictEqual($(ths).eq(0).find("i").attr("class"), "fa fa-sort");
        assert.strictEqual($(ths).eq(1).data("order"), 1);
        assert.strictEqual($(ths).eq(1).find("i").attr("class"), "fa fa-sort-asc");
        assert.strictEqual($(ths).eq(2).data("order"), 0);
        assert.strictEqual($(ths).eq(2).find("i").attr("class"), "fa fa-sort");

        assert.deepEqual(
            $.publishings,
            [
                { name : "result-table.sort", data : [0, 1] },
                { name : "result-table.sort", data : [1, 1] },
            ]
        );
    });

}(jQuery));
