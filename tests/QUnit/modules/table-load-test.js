(function ($) {
    "use strict";

    QUnit.module("table-load", {
        beforeEach: function () {
            $.test.publish("view.fastaOutputUrl", undefined);
        },
        afterEach: function () {
            $.test.afterEach();
        }
    });

    QUnit.test("activate", function (assert) {
        assert.deepEqual($.subscriptions, ["table-load.load", "view.fastaOutputUrl"]);
        assert.deepEqual($.publishings, []);
    });

    QUnit.test("load", function (assert) {
        var done = assert.async();

        $.mockjax({
            url: "/path/to/fasta",
            responseText: "4\tTest\t2x\n-12.4\t123,234\tAbc\n"
        });

        $.test.publish("view.fastaOutputUrl", "/path/to/fasta");

        $.test.publish("table-load.load");

        assert.strictEqual($.mockjax.mockedAjaxCalls().length, 1);
        assert.strictEqual($.mockjax.unmockedAjaxCalls().length, 0);

        setTimeout(function () {
            assert.deepEqual(
                $.publishings,
                [
                    "overlay.show",
                    {
                        name: "result-table.init",
                        data: [
                            [
                                [
                                    { rawValue: "4",       value: 4 },
                                    { rawValue: "Test",    value: "Test" },
                                    { rawValue: "2x",      value: 2 },
                                ],
                                [
                                    { rawValue: "-12.4",   value: -12.4 },
                                    { rawValue: "123,234", value: 123 },
                                    { rawValue: "Abc",     value: "Abc" },
                                ],
                            ],
                        ],
                    },
                    "overlay.hide",
                ]
            );
            done();
        }, 100);
    });

    QUnit.test("load without view events", function (assert) {
        $.test.publish("table-load.load");

        assert.strictEqual($.mockjax.mockedAjaxCalls().length, 0);
        assert.strictEqual($.mockjax.unmockedAjaxCalls().length, 0);

        assert.deepEqual($.publishings, []);
    });

}(jQuery));
