(function ($) {
    "use strict";

    QUnit.module("fasta-load", {
        beforeEach: function () {
            $.test.publish("refresh-page.timeout", -1);
        },
        afterEach: function () {
            $.test.afterEach();
        }
    });

    QUnit.test("activate", function (assert) {
        assert.deepEqual($.subscriptions, ["refresh-page.init", "refresh-page.location", "refresh-page.timeout"]);
        assert.deepEqual($.publishings, []);
    });

    QUnit.test("refresh", function (assert) {
        var done    = assert.async();
        var counter = 0;

        var location = {
            reload: function () {
                counter++;
            }
        };

        $.test.publish("refresh-page.location", location);

        $.test.publish("refresh-page.timeout", 1);

        $.test.publish("refresh-page.init");

        setTimeout(function () {
            assert.strictEqual(counter, 1);
            done();
        }, 1100);
    });

    QUnit.test("refresh with invalid timeout", function (assert) {
        var done    = assert.async();
        var counter = 0;

        var location = {
            reload: function () {
                counter++;
            }
        };

        $.test.publish("refresh-page.location", location);

        $.test.publish("refresh-page.timeout", -1);

        $.test.publish("refresh-page.init");

        setTimeout(function () {
            assert.strictEqual(counter, 0);
            done();
        }, 1100);
    });

}(jQuery));
