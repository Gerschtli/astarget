#!/bin/bash

set -e

WORK_DIR="$(dirname $(readlink -f "${0}"))"

GIT_URL="https://gitlab.com/thoelken/astarget.git"
GIT_BRANCH="master"

GIT_DIR="${WORK_DIR}/project"
WORKSPACE_DIR="${WORK_DIR}/workspace"

INSTALL_DIR="/home/astarget/html"
SHARED_VAR_DIR="${INSTALL_DIR}/var"

BUILD=$(date '+%s')
MAX_BACKUPS=2

if [ -f "${WORK_DIR}/install.conf" ]; then
    source "${WORK_DIR}/install.conf"
fi

_log() {
    if [ "${2}" = "ERROR" ]; then
        echo -en "\033[31m"
    else
        echo -en "\033[32m"
    fi
    echo ${1}
    echo -en "\033[0m"

    if [ "${2}" = "ERROR" ]; then
        _log "aborting ..."
        exit 1
    fi
}
export -f _log

_test() {
    if [ ! -w "${INSTALL_DIR}" ]; then
        _log "${INSTALL_DIR} has to be writable!" "ERROR"
    fi
}

_export() {
    if [ ! -d "${GIT_DIR}" ]; then
        _log "cloning the repository ..."
        git clone "${GIT_URL}" "${GIT_DIR}"
    fi

    pushd "${GIT_DIR}" 1> /dev/null
    _log "get latest code base [git pull origin ${GIT_BRANCH}] ..."
    git fetch --prune
    git checkout -f ${GIT_BRANCH}
    git pull origin ${GIT_BRANCH}
    popd 1> /dev/null
}

_copyParameters() {
    _log "copy parameters.yml ..."
    cp "${WORK_DIR}/parameters.yml" "${GIT_DIR}/app/config/parameters.yml"
}

_composer() {
    pushd "${GIT_DIR}" 1> /dev/null
    if [ ! -f bin/composer.phar ]; then
        _log "downloading composer ..."
        curl -sS "https://getcomposer.org/installer" | php -- --install-dir=bin
    else
        _log "composer update ..."
        php bin/composer.phar self-update
    fi

    php bin/composer.phar install --prefer-source --no-dev --optimize-autoloader
    popd 1> /dev/null
}

_build() {
    if [ -d "${WORKSPACE_DIR}" ]; then
        _log "remove old workspace ... "
        rm -rf "${WORKSPACE_DIR}"
    fi
    _log "creating workspace ... "
    mkdir -p "${WORKSPACE_DIR}"

    _log "install code ..."
    cp -r "${GIT_DIR}/"* "${WORKSPACE_DIR}"
}

_prepareApp() {
    pushd "${WORKSPACE_DIR}" 1> /dev/null
    _log "migrating database ..."
    php bin/console doctrine:migrations:migrate --no-interaction latest

    _log "building assets ..."
    php bin/console assetic:dump
    popd 1> /dev/null
}

_linkVar() {
    if [ ! -d "${SHARED_VAR_DIR}/data" -o ! -d "${SHARED_VAR_DIR}/logs" ]; then
        _log "creating shared var dirs ..."
        mkdir -p "${SHARED_VAR_DIR}/"{data,logs}
    fi

    _log "link shared var dirs ..."
    rm -r "${WORKSPACE_DIR}/var/"{cache,data,logs,sessions}

    mkdir -p "${WORKSPACE_DIR}/var/"{cache,sessions}
    ln -snf "${SHARED_VAR_DIR}/data" "${WORKSPACE_DIR}/var"
    ln -snf "${SHARED_VAR_DIR}/logs" "${WORKSPACE_DIR}/var"

    _log "chmod of var dirs ..."
    chmod 0775 "${WORKSPACE_DIR}/var/"{cache,data,logs,sessions}
}

_cleanSource() {
    _log 'cleaning workspace ...'
    rm -r "${WORKSPACE_DIR}/ci" \
        "${WORKSPACE_DIR}/cli-workflow" \
        "${WORKSPACE_DIR}/features" \
        "${WORKSPACE_DIR}/tests" \
        "${WORKSPACE_DIR}/vagrant" \
        "${WORKSPACE_DIR}/web/bundles"

    rm -rf "${WORKSPACE_DIR}/.git"

    find "${WORKSPACE_DIR}" -maxdepth 1 -type f -exec rm {} \+
}

_move() {
    _log 'moving the source to ...'
    if [ ! -d "${INSTALL_DIR}" ]; then
        _log 'create install dir ...'
        mkdir -p "${INSTALL_DIR}"
    fi
    mv "${WORKSPACE_DIR}" "${INSTALL_DIR}/BUILD-${BUILD}"
}

_link() {
    _log 'create a symbolic link ...'
    ln -snf "${INSTALL_DIR}/BUILD-${BUILD}" "${INSTALL_DIR}/current"
}

_cleanup() {
    _log 'deleting old backup files ...'
    for BUILD in $(find "${INSTALL_DIR}" -maxdepth 1 -type d -name "BUILD-*" | sort -r | tail -n +$(( ${MAX_BACKUPS} + 2))); do
        rm -rf "${BUILD}"
    done
}

_test
_export
_copyParameters
_composer
_build
_prepareApp
_linkVar
_cleanSource
_move
_link
_cleanup

exit 0
